package com.example.list_avata;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
public class MainActivity extends AppCompatActivity implements IOnChildItemClick{
    private List<ContactModel> listContact = new ArrayList<>();
    private ListView lvContact;
    private ContactAdapter mAdapter;
    private ImageView ivUser;
    private TextView tvName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        initView();
        mAdapter = new ContactAdapter(this,listContact);
        mAdapter.registerChildItemClick(this);
        lvContact.setAdapter(mAdapter);
        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ContactModel model = listContact.get(i);
                Toast.makeText(MainActivity.this,model.getName()+": "+model.getPhone(),Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void initView(){
        lvContact = (ListView)findViewById(R.id.lvContact);
        ivUser = (ImageView)findViewById(R.id.ivUser);
        tvName = (TextView)findViewById(R.id.tvName);
    }
    private void initData(){
        listContact.add(new ContactModel("Nguyen Tien Duoc","012345678",R.drawable.ic_u1));
        listContact.add(new ContactModel("Dinh thu Trang","0216362334",R.drawable.ic_u2));
        listContact.add(new ContactModel("Nguyen Van Quoc","0327152672",R.drawable.ic_u3));
        listContact.add(new ContactModel("Pham Kieu Trang","0356256536",R.drawable.ic_u4));
        listContact.add(new ContactModel("Dinh Trung Hieu","0376732763",R.drawable.ic_u5));
        listContact.add(new ContactModel("Doan Thi Huong","0367263763",R.drawable.ic_u1));
        listContact.add(new ContactModel("Pham Thi Thu","02637263723",R.drawable.ic_u2));
        listContact.add(new ContactModel("Pham Minh Quy","02376237233",R.drawable.ic_u3));
        listContact.add(new ContactModel("Doan Duc Trong","02367263263",R.drawable.ic_u4));
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        mAdapter.unRegisterChildItemClick();
    }
    @Override
    public void onItemChildClick(int positon){
        ContactModel contact = listContact.get(positon);
        ivUser.setImageResource(contact.getImage());
        tvName.setText(contact.getName());
    }
}
